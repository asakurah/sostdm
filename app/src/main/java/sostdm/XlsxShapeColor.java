package sostdm;

import java.util.regex.Pattern;
import java.util.function.Function;
import java.util.function.Consumer;

record XlsxShapeColor(int red, int green, int blue) {

    private static Pattern hex6 = Pattern.compile("#?([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})([0-9A-Fa-f]{2})");

    public static XlsxShapeColor from(String src) {
        {
            var m = hex6.matcher(src);
            if (m.matches()) {
                return new XlsxShapeColor(Integer.parseInt(m.group(1), 16), Integer.parseInt(m.group(2), 16),
                        Integer.parseInt(m.group(3), 16));
            }
        }

        throw new RuntimeException("Unsupported format: " + src);
    }

    public void apply(Function<Integer, Function<Integer, Consumer<Integer>>> func) {
        func.apply(this.red).apply(this.green).accept(this.blue);
    }

}