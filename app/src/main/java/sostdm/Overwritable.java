package sostdm;

interface Overwritable<T> {
    T overwrite(T obj);

    public static <X extends Overwritable<X>> X overwrite(X origin, X other) {
        if (origin == null) {
            return other;
        }

        if (other == null) {
            return origin;
        }

        return origin.overwrite(other);
    }
}
