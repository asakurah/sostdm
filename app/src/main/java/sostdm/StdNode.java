package sostdm;

import java.util.LinkedHashMap;

public class StdNode {
    public String name;
    public String key;
    public int index;
    public int endIndex;
    public int height;
    public boolean isSeeOthers;
    public AttrSet attrSet = new AttrSet();
    public LinkedHashMap<StdRelation, StdNode> children = new LinkedHashMap<StdRelation, StdNode>();
}
