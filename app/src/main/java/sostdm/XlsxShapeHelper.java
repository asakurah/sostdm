package sostdm;

import java.util.function.Consumer;

import org.apache.poi.xssf.usermodel.XSSFConnector;
import org.openxmlformats.schemas.drawingml.x2006.main.STPresetLineDashVal;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndType;

class XlsxShapeHelper {

    public static void setLineEndType(XSSFConnector obj, STLineEndType.Enum line_end_type) {
        if (obj == null || line_end_type == null) {
            return;
        }

        var line_end_type_val = STLineEndType.Factory.newValue(line_end_type);
        obj.getCTConnector().getSpPr().getLn().addNewTailEnd().xsetType(line_end_type_val);
    }

    public static void setAdj1(XSSFConnector obj, XlsxConnectorPercent val) {
        if (obj == null || val == null) {
            return;
        }

        var spPr = obj.getCTConnector().getSpPr();
        var prstGeom = spPr.isSetPrstGeom() ? spPr.getPrstGeom() : spPr.addNewPrstGeom();
        var avLst = prstGeom.isSetAvLst() ? prstGeom.getAvLst() : prstGeom.addNewAvLst();
        var gd = avLst.addNewGd();
        gd.setName("adj1");
        gd.setFmla("val " + val.value());
    }

    public static void applyLineStyle(STPresetLineDashVal.Enum line_style, Consumer<Integer> consumer) {
        if (line_style != null) {
            consumer.accept(line_style.intValue() - 1);
        }
    }

}
