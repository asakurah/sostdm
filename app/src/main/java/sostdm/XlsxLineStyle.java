package sostdm;

import org.apache.commons.lang3.ObjectUtils;

import org.openxmlformats.schemas.drawingml.x2006.main.STPresetLineDashVal;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndType;
import org.apache.poi.xssf.usermodel.XSSFConnector;

record XlsxLineStyle(XlsxShapeColor line_color, STPresetLineDashVal.Enum line_style, Double line_width,
        STLineEndType.Enum line_end_type) implements Overwritable<XlsxLineStyle> {

    public XlsxLineStyle overwrite(XlsxLineStyle obj) {
        if (obj == null) {
            return this;
        }

        return new XlsxLineStyle(ObjectUtils.defaultIfNull(obj.line_color, this.line_color),
                ObjectUtils.defaultIfNull(obj.line_style, this.line_style),
                ObjectUtils.defaultIfNull(obj.line_width, this.line_width),
                ObjectUtils.defaultIfNull(obj.line_end_type, this.line_end_type));
    }

    public void apply(XSSFConnector obj) {
        ObjUtils.ifPresent(this.line_color, x -> x.apply(r -> g -> b -> obj.setLineStyleColor(r, g, b)));
        XlsxShapeHelper.applyLineStyle(this.line_style, x -> obj.setLineStyle(x));
        ObjUtils.ifPresent(this.line_width, x -> obj.setLineWidth(x));
        ObjUtils.ifPresent(this.line_end_type, x -> XlsxShapeHelper.setLineEndType(obj, x));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private XlsxShapeColor line_color;
        private STPresetLineDashVal.Enum line_style;
        private Double line_width;
        private STLineEndType.Enum line_end_type;

        public Builder() {
        }

        public XlsxLineStyle build() {
            return new XlsxLineStyle(this.line_color, this.line_style, this.line_width, this.line_end_type);
        }

        public Builder line_color(XlsxShapeColor v) {
            this.line_color = v;
            return this;
        }

        public Builder line_style(STPresetLineDashVal.Enum v) {
            this.line_style = v;
            return this;
        }

        public Builder line_width(Double v) {
            this.line_width = v;
            return this;
        }

        public Builder line_end_type(STLineEndType.Enum v) {
            this.line_end_type = v;
            return this;
        }
    }
}
