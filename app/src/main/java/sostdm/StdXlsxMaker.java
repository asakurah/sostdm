package sostdm;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.ClientAnchor.AnchorType;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.FontUnderline;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFGraphicFrame;
import org.apache.poi.xssf.usermodel.XSSFShapeGroup;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFConnector;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFTextBox;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.TextAlign;
import org.apache.poi.xssf.usermodel.TextHorizontalOverflow;
import org.apache.poi.xssf.usermodel.TextVerticalOverflow;

import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndType;
import org.openxmlformats.schemas.drawingml.x2006.main.STPresetLineDashVal;

public class StdXlsxMaker implements AutoCloseable {

    // configurations

    private Integer new_sheet_default_col_width = 3;
    private Short new_sheet_default_row_height = null;

    private int start_pos_col = 1;
    private int start_pos_row = 12;

    private int screen_box_width = 6; // number of cells
    private int screen_box_height = 4; // number of cells
    private AnchorType screen_box_anchor_type = AnchorType.MOVE_AND_RESIZE;

    private XlsxShapeStyle screen_box_default_style = XlsxShapeStyle.builder().fill_color(XlsxShapeColor.from("FFFFFF"))
            .line_color(XlsxShapeColor.from("000000")).line_style(STPresetLineDashVal.SOLID).line_width(1.0)
            .font(XlsxFontStyle.builder().font_name("Meiryo UI").font_height(XlsxFontHeight.from(11.0)).bold(false)
                    .italic(false).strikeout(false).underline(FontUnderline.NONE).build())
            .h_align(TextAlign.CENTER).v_align(VerticalAlignment.CENTER).h_overflow(TextHorizontalOverflow.CLIP)
            .v_overflow(TextVerticalOverflow.CLIP).build();
    private XlsxShapeStyle screen_box_see_other_style = XlsxShapeStyle.builder().line_style(STPresetLineDashVal.DOT)
            .build();
    private List<Map<String, XlsxShapeStyle>> screen_box_attr_style = new ArrayList<Map<String, XlsxShapeStyle>>() {
        {
            add(new HashMap<String, XlsxShapeStyle>() {
                {
                    put("新規", XlsxShapeStyle.builder().fill_color(XlsxShapeColor.from("FFC0C0")).build());
                    put("変更", XlsxShapeStyle.builder().fill_color(XlsxShapeColor.from("C0FFC0")).build());
                    put("削除", XlsxShapeStyle.builder().fill_color(XlsxShapeColor.from("C0C0C0")).build());
                }
            });
        }
    };

    private boolean screen_remark_is_in_cell = true;

    private int box_space_width = 10; // number of cells
    private int box_space_height = 2; // number of cells

    private AnchorType connector_anchor_type = AnchorType.MOVE_AND_RESIZE;
    private XlsxConnectorPercent connector_adjust = XlsxConnectorPercent.from(20.0);

    private AnchorType event_box_anchor_type = AnchorType.MOVE_AND_RESIZE;

    private XlsxConnectorStyle transition_line_default_style = new XlsxConnectorStyle(
            XlsxLineStyle.builder().line_color(XlsxShapeColor.from("000000")).line_style(STPresetLineDashVal.SOLID)
                    .line_width(1.0).line_end_type(STLineEndType.TRIANGLE).build(),
            XlsxShapeStyle.builder().line_width(0.0)
                    .font(XlsxFontStyle.builder().font_name("Meiryo UI").font_height(XlsxFontHeight.from(12.0)).build())
                    .h_align(TextAlign.LEFT).v_align(VerticalAlignment.BOTTOM).h_overflow(TextHorizontalOverflow.CLIP)
                    .v_overflow(TextVerticalOverflow.OVERFLOW).build());
    private List<Map<String, XlsxConnectorStyle>> transition_line_attr_style = new ArrayList<Map<String, XlsxConnectorStyle>>() {
        {
            add(new HashMap<String, XlsxConnectorStyle>() {
                {
                    put("1型",
                            new XlsxConnectorStyle(
                                    XlsxLineStyle.builder().line_style(STPresetLineDashVal.SYS_DASH).build(),
                                    XlsxShapeStyle.builder().fill_color(XlsxShapeColor.from("C00000")).build()));
                    put("2型",
                            new XlsxConnectorStyle(XlsxLineStyle.builder().line_style(STPresetLineDashVal.DASH).build(),
                                    XlsxShapeStyle.builder().fill_color(XlsxShapeColor.from("00C000")).build()));
                }
            });
        }
    };

    private boolean transition_event_is_in_cell = true;
    private boolean transition_remark_is_in_cell = true;

    private XlsxFontStyle remark_font = XlsxFontStyle.builder().font_name("Meiryo UI")
            .font_height(XlsxFontHeight.from(11.0)).bold(false).italic(false).strikeout(false)
            .underline(FontUnderline.NONE).build();

    // internal status

    private Workbook curWb = null;
    private Sheet curSheet = null;
    private long nextObjectId = 2;
    private XlsxStyleManager style_manager = null;
    private WordWrapperForJapanese waord_wrapper = new WordWrapperForJapanese();

    private record XlsxConnectorStyle(XlsxLineStyle line_style, XlsxShapeStyle box_style)
            implements Overwritable<XlsxConnectorStyle> {

        public XlsxConnectorStyle overwrite(XlsxConnectorStyle obj) {
            if (obj == null) {
                return this;
            } else {
                return new XlsxConnectorStyle(Overwritable.overwrite(this.line_style, obj.line_style),
                        Overwritable.overwrite(this.box_style, obj.box_style));
            }
        }
    }

    private record CellRect(int col1, int row1, int col2, int row2) {
    }

    public StdXlsxMaker() {
    }

    public void createNewBook() {
        try {
            curWb = WorkbookFactory.create(true);
            style_manager = new XlsxStyleManager(curWb);
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void addNewSheet() {
        if (curWb == null) {
            throw new RuntimeException("No Workbook");
        }

        try {
            curSheet = curWb.createSheet("Screen Transition Diagram");

            if (new_sheet_default_col_width != null) {
                curSheet.setDefaultColumnWidth(new_sheet_default_col_width);
            }

            if (new_sheet_default_row_height != null) {
                curSheet.setDefaultRowHeight(new_sheet_default_row_height);
            }

            PoiSsHelper.setStrCellVal(curSheet, 0, 0, "Screen Transition Diagram");
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void loadBook(InputStream istrm) {
        try {
            curWb = new XSSFWorkbook(istrm);
            style_manager = new XlsxStyleManager(curWb);
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void selectSheet(String name) {
        if (curWb == null) {
            throw new RuntimeException("No Workbook");
        }

        try {
            curSheet = curWb.getSheet(name);
            initNextObjectId();
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void selectSheetAt(int index) {
        if (curWb == null) {
            throw new RuntimeException("No Workbook");
        }

        try {
            curSheet = curWb.getSheetAt(index);
            initNextObjectId();
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    protected void initNextObjectId() {
        var drawing = (XSSFDrawing) curSheet.createDrawingPatriarch();

        long max_id = -1;
        for (XSSFShape shape : drawing.getShapes()) {
            long cur_id = getIdFromShape(shape);
            if (max_id < cur_id) {
                max_id = cur_id;
            }
        }

        if (max_id > 0) {
            this.nextObjectId = max_id + 1;
        }
    }

    protected long getIdFromShape(XSSFShape shape) {
        if (shape instanceof XSSFConnector) {
            var connector = (XSSFConnector) shape;
            return connector.getCTConnector().getNvCxnSpPr().getCNvPr().getId();

        } else if (shape instanceof XSSFGraphicFrame) {
            var gf = (XSSFGraphicFrame) shape;
            return gf.getId();

        } else if (shape instanceof XSSFShapeGroup) {
            var sg = (XSSFShapeGroup) shape;
            return sg.getCTGroupShape().getNvGrpSpPr().getCNvPr().getId();

        } else if (shape instanceof XSSFPicture) {
            var picture = (XSSFPicture) shape;
            return picture.getCTPicture().getNvPicPr().getCNvPr().getId();

        } else if (shape instanceof XSSFSimpleShape) {
            var ss = (XSSFSimpleShape) shape;
            return ss.getCTShape().getNvSpPr().getCNvPr().getId();
            // return ss.getShapeId(); // Why return value of getShapeId() cast as int?

        } else {
            return -1;
        }
    }

    public void addScreenTransitionDiagram(StdNode tree) {
        if (curSheet == null) {
            throw new RuntimeException("No Sheet");
        }

        try {

            putNode(start_pos_col, start_pos_row, tree);

        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    protected XSSFTextBox putNode(int col, int row, StdNode tree) {
        var box1 = addScreen(col, row, tree.name, tree.isSeeOthers, tree.attrSet);

        col += screen_box_width + box_space_width;

        for (var pair : tree.children.entrySet()) {
            StdRelation rel = pair.getKey();
            StdNode child = pair.getValue();

            var box2 = putNode(col, row, child);

            addTransitionLine(box1, box2, rel);

            row += (screen_box_height + box_space_height) * child.height;
        }

        return box1;
    }

    protected XSSFTextBox addScreen(int col, int row, String name, boolean isSeeOthers, AttrSet attr_set) {
        var col2 = col + this.screen_box_width;
        var row2 = row + this.screen_box_height;
        var tb = addScreenBox(col, row, col2, row2, name, isSeeOthers, attr_set);
        if (attr_set != null) {
            var remark_row = row2 - 1;
            if (this.screen_remark_is_in_cell) {
                addRemarkOfScreenInCell(col2, remark_row, attr_set.remark);
            } else {
                addRemarkOfScreen(col2, remark_row, attr_set.remark);
            }
        }
        return tb;
    }

    protected XSSFTextBox addScreenBox(int col1, int row1, int col2, int row2, String name, boolean isSeeOthers,
            AttrSet attr_set) {
        var drawing = (XSSFDrawing) curSheet.createDrawingPatriarch();

        var anchor = drawing.createAnchor(0, 0, 0, 0, col1, row1, col2, row2);
        anchor.setAnchorType(screen_box_anchor_type);

        var tb = drawing.createTextbox(anchor);
        setObjectId(id -> tb.getCTShape().getNvSpPr().getCNvPr().setId(id));

        XlsxShapeStyle style = this.screen_box_default_style;
        if (attr_set != null) {
            style = attr_set.<XlsxShapeStyle>overwriteByTypes(this.screen_box_attr_style, style);
        }
        if (isSeeOthers) {
            style = style.overwrite(this.screen_box_see_other_style);
        }

        this.style_manager.applyStyleAndText(tb, style, name);

        return tb;
    }

    protected XSSFTextBox addRemarkOfScreen(int col, int row, String text) {
        if (text == null) {
            return null;
        }

        var drawing = (XSSFDrawing) curSheet.createDrawingPatriarch();

        var anchor = drawing.createAnchor(0, 0, 0, 0, col, row, col + 1, row + 1);
        anchor.setAnchorType(this.event_box_anchor_type);

        var tb = drawing.createTextbox(anchor);
        setObjectId(id -> tb.getCTShape().getNvSpPr().getCNvPr().setId(id));

        tb.setLineWidth(0.0);

        tb.setTextHorizontalOverflow(TextHorizontalOverflow.OVERFLOW);
        tb.setTextVerticalOverflow(TextVerticalOverflow.OVERFLOW);

        tb.clearText();

        var paragraph = this.style_manager.addNewTextParagraph(tb, this.remark_font, text);
        paragraph.setTextAlign(TextAlign.LEFT);

        tb.setVerticalAlignment(VerticalAlignment.BOTTOM);

        return tb;
    }

    protected void addRemarkOfScreenInCell(int col, int row, String text) {
        if (text == null) {
            return;
        }

        var lines = text.split("\n|\r|\r\n");
        int row_pos = row - (lines.length - 1);
        if (row_pos < this.start_pos_row) {
            row_pos = this.start_pos_row;
        }

        var font = this.remark_font != null ? this.style_manager.getFont(this.remark_font) : null;

        for (var line : lines) {
            var rich_txt = new XSSFRichTextString(line);
            if (font != null) {
                rich_txt.applyFont(font);
            }
            PoiSsHelper.setStrCellVal(this.curSheet, col, row_pos, rich_txt);
            ++row_pos;
        }

        return;
    }

    protected void addTransitionLine(XSSFSimpleShape from, XSSFSimpleShape to, StdRelation rel) {
        XlsxConnectorStyle style = transition_line_default_style;
        if (rel.attr_set != null) {
            style = rel.attr_set.overwriteByTypes(this.transition_line_attr_style, style);
        }

        addConnector(from, to, style.line_style);

        var event_rect = calcCellRect(to);
        if (this.transition_event_is_in_cell) {
            addEventInCell(event_rect, rel.event, style.box_style);
        } else {
            addEvent(event_rect, rel.event, style.box_style);
        }

        if (rel.attr_set != null) {
            if (this.transition_remark_is_in_cell) {
                addRemarkOfTransitionInCell(event_rect, rel.attr_set.remark);
            } else {
                addRemarkOfTransition(event_rect, rel.attr_set.remark);
            }
        }
    }

    protected XSSFConnector addConnector(XSSFSimpleShape from, XSSFSimpleShape to, XlsxLineStyle style) {
        var from_anchor = (XSSFClientAnchor) from.getAnchor();
        var to_anchor = (XSSFClientAnchor) to.getAnchor();

        var col1 = from_anchor.getCol2();
        var row1 = calcConter(from_anchor.getRow1(), from_anchor.getRow2());

        var col2 = to_anchor.getCol1();
        var row2 = calcConter(to_anchor.getRow1(), to_anchor.getRow2());

        var drawing = (XSSFDrawing) curSheet.createDrawingPatriarch();

        var anchor = drawing.createAnchor(0, 0, 0, 0, col1, row1, col2, row2);
        anchor.setAnchorType(connector_anchor_type);

        var connector = drawing.createConnector(anchor);
        setObjectId(id -> connector.getCTConnector().getNvCxnSpPr().getCNvPr().setId(id));

        connector.getCTConnector().getNvCxnSpPr().getCNvCxnSpPr().addNewStCxn()
                .setId(from.getCTShape().getNvSpPr().getCNvPr().getId());
        connector.getCTConnector().getNvCxnSpPr().getCNvCxnSpPr().getStCxn().setIdx(3); // 0:top, 1:left, 2:bottom,
                                                                                        // 3:right
        connector.getCTConnector().getNvCxnSpPr().getCNvCxnSpPr().addNewEndCxn()
                .setId(to.getCTShape().getNvSpPr().getCNvPr().getId());
        connector.getCTConnector().getNvCxnSpPr().getCNvCxnSpPr().getEndCxn().setIdx(1);

        connector.setShapeType(ShapeTypes.BENT_CONNECTOR_3);

        if (this.connector_adjust != null) {
            XlsxShapeHelper.setAdj1(connector, this.connector_adjust);
        }

        style.apply(connector);

        return connector;
    }

    protected XSSFTextBox addEvent(CellRect rect, String name, XlsxShapeStyle style) {
        if (name == null) {
            return null;
        }

        var drawing = (XSSFDrawing) curSheet.createDrawingPatriarch();

        var anchor = drawing.createAnchor(0, 0, 0, 0, rect.col1(), rect.row1(), rect.col2(), rect.row2());
        anchor.setAnchorType(event_box_anchor_type);

        var tb = drawing.createTextbox(anchor);
        setObjectId(id -> tb.getCTShape().getNvSpPr().getCNvPr().setId(id));

        this.style_manager.applyStyleAndText(tb, style, name);

        return tb;
    }

    protected CellRect addEventInCell(CellRect rect, String text, XlsxShapeStyle style) {
        if (text == null) {
            return rect;
        }

        var line_width = PoiSsHelper.getColumnsWidthAsNumOfChrs(this.curSheet, rect.col1(), rect.col2());
        var lines = this.waord_wrapper.wordWrap(text, line_width, 1.5);
        int row_pos = rect.row2() - lines.size();
        if (row_pos < this.start_pos_row) {
            row_pos = this.start_pos_row;
        }

        var new_rect = new CellRect(rect.col1(), row_pos, rect.col2(), row_pos + lines.size());

        var font = (style != null && style.font() != null) ? this.style_manager.getFont(style.font()) : null;

        for (var line : lines) {
            var rich_txt = new XSSFRichTextString(line);
            if (font != null) {
                rich_txt.applyFont(font);
            }
            PoiSsHelper.setStrCellVal(this.curSheet, new_rect.col1(), row_pos, rich_txt);
            ++row_pos;
        }

        return new_rect;
    }

    protected XSSFTextBox addRemarkOfTransition(CellRect rect, String text) {
        if (text == null) {
            return null;
        }

        var drawing = (XSSFDrawing) curSheet.createDrawingPatriarch();

        var anchor = drawing.createAnchor(0, 0, 0, 0, rect.col1(), rect.row2(), rect.col2(), rect.row2() + 1);
        anchor.setAnchorType(event_box_anchor_type);

        var tb = drawing.createTextbox(anchor);
        setObjectId(id -> tb.getCTShape().getNvSpPr().getCNvPr().setId(id));

        tb.setLineWidth(0.0);

        tb.setTextHorizontalOverflow(TextHorizontalOverflow.OVERFLOW);
        tb.setTextVerticalOverflow(TextVerticalOverflow.OVERFLOW);

        tb.clearText();

        var paragraph = this.style_manager.addNewTextParagraph(tb, this.remark_font, text);
        paragraph.setTextAlign(TextAlign.LEFT);

        tb.setVerticalAlignment(VerticalAlignment.TOP);

        return tb;
    }

    protected CellRect addRemarkOfTransitionInCell(CellRect rect, String text) {
        if (text == null) {
            return rect;
        }

        var line_width = PoiSsHelper.getColumnsWidthAsNumOfChrs(this.curSheet, rect.col1(), rect.col2());
        var lines = this.waord_wrapper.wordWrap(text, line_width, 1.5);
        int row_pos = rect.row2();

        var new_rect = new CellRect(rect.col1(), row_pos, rect.col2(), row_pos + lines.size());

        var font = this.remark_font != null ? this.style_manager.getFont(this.remark_font) : null;

        for (var line : lines) {
            var rich_txt = new XSSFRichTextString(line);
            if (font != null) {
                rich_txt.applyFont(font);
            }
            PoiSsHelper.setStrCellVal(this.curSheet, new_rect.col1(), row_pos, rich_txt);
            ++row_pos;
        }

        return new_rect;
    }

    protected CellRect calcCellRect(XSSFSimpleShape to) {
        var to_anchor = (XSSFClientAnchor) to.getAnchor();

        int col2 = to_anchor.getCol1();
        int row2 = calcConter(to_anchor.getRow1(), to_anchor.getRow2());

        int col1 = to_anchor.getCol1()
                - (this.box_space_width - (int) Math.round(this.box_space_width * this.connector_adjust.toRate())) + 1;
        int row1 = row2 - 1;

        return new CellRect(col1, row1, col2, row2);
    }

    protected int calcConter(int a, int b) {
        return a + ((b - a) / 2);
    }

    protected void setObjectId(Consumer<Long> func) {
        func.accept(this.nextObjectId);
        this.nextObjectId++;
    }

    public void save(String output_path) {
        if (this.curWb == null) {
            return;
        }

        try {
            Path dst_path = FileSystems.getDefault().getPath(output_path);
            Files.createDirectories(dst_path.getParent());
            try (OutputStream fos = Files.newOutputStream(dst_path)) {
                curWb.write(fos);
                fos.close();
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void close() throws RuntimeException {
        if (curWb == null) {
            return;
        }

        try {
            curWb.close();
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

}
