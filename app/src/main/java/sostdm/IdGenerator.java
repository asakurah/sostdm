package sostdm;

class IdGenerator {
    private int id;

    public IdGenerator() {
        this.id = 0;
    }

    public IdGenerator(int init) {
        this.id = init;
    }

    public int next() {
        var ret_id = this.id;
        this.id = this.id + 1;
        return ret_id;
    }

    public int current() {
        return this.id;
    }
}