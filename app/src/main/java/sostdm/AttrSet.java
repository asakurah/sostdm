package sostdm;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

class AttrSet {
    public List<String> types = new ArrayList<String>();
    public String remark;

    public <X extends Overwritable<X>> X overwriteByTypes(List<Map<String, X>> map_list, X origin) {
        X style = origin;
        if (map_list != null && this.types != null) {
            int i = 0;
            for (var type : this.types) {
                if (i < map_list.size()) {
                    var style_map = map_list.get(i);
                    if (style_map != null) {
                        style = style.overwrite(style_map.get(type));
                    }
                }
                ++i;
            }
        }
        return style;
    }

}
