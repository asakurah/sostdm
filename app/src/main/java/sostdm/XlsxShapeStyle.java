package sostdm;

import org.apache.commons.lang3.ObjectUtils;

import org.openxmlformats.schemas.drawingml.x2006.main.STPresetLineDashVal;
import org.apache.poi.xssf.usermodel.TextAlign;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.TextHorizontalOverflow;
import org.apache.poi.xssf.usermodel.TextVerticalOverflow;

record XlsxShapeStyle(String force_text, XlsxShapeColor fill_color, XlsxShapeColor line_color,
        STPresetLineDashVal.Enum line_style, Double line_width, XlsxFontStyle font, TextAlign h_align,
        VerticalAlignment v_align, TextHorizontalOverflow h_overflow, TextVerticalOverflow v_overflow)
        implements Overwritable<XlsxShapeStyle> {

    public XlsxShapeStyle overwrite(XlsxShapeStyle obj) {
        if (obj == null) {
            return this;
        }

        return new XlsxShapeStyle(ObjectUtils.defaultIfNull(obj.force_text, this.force_text),
                ObjectUtils.defaultIfNull(obj.fill_color, this.fill_color),
                ObjectUtils.defaultIfNull(obj.line_color, this.line_color),
                ObjectUtils.defaultIfNull(obj.line_style, this.line_style),
                ObjectUtils.defaultIfNull(obj.line_width, this.line_width), Overwritable.overwrite(this.font, obj.font),
                ObjectUtils.defaultIfNull(obj.h_align, this.h_align),
                ObjectUtils.defaultIfNull(obj.v_align, this.v_align),
                ObjectUtils.defaultIfNull(obj.h_overflow, this.h_overflow),
                ObjectUtils.defaultIfNull(obj.v_overflow, this.v_overflow));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String force_text;
        private XlsxShapeColor fill_color;
        private XlsxShapeColor line_color;
        private STPresetLineDashVal.Enum line_style;
        private Double line_width;
        private XlsxFontStyle font;
        private TextAlign h_align;
        private VerticalAlignment v_align;
        private TextHorizontalOverflow h_overflow;
        private TextVerticalOverflow v_overflow;

        public Builder() {
        }

        public XlsxShapeStyle build() {
            return new XlsxShapeStyle(this.force_text, this.fill_color, this.line_color, this.line_style,
                    this.line_width, this.font, this.h_align, this.v_align, this.h_overflow, this.v_overflow);
        }

        public Builder force_text(String v) {
            this.force_text = v;
            return this;
        }

        public Builder fill_color(XlsxShapeColor v) {
            this.fill_color = v;
            return this;
        }

        public Builder line_color(XlsxShapeColor v) {
            this.line_color = v;
            return this;
        }

        public Builder line_style(STPresetLineDashVal.Enum v) {
            this.line_style = v;
            return this;
        }

        public Builder line_width(Double v) {
            this.line_width = v;
            return this;
        }

        public Builder font(XlsxFontStyle v) {
            this.font = v;
            return this;
        }

        public Builder h_align(TextAlign v) {
            this.h_align = v;
            return this;
        }

        public Builder v_align(VerticalAlignment v) {
            this.v_align = v;
            return this;
        }

        public Builder h_overflow(TextHorizontalOverflow v) {
            this.h_overflow = v;
            return this;
        }

        public Builder v_overflow(TextVerticalOverflow v) {
            this.v_overflow = v;
            return this;
        }

    }
}
