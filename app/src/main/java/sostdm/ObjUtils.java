package sostdm;

import java.util.function.Consumer;

class ObjUtils {
    public static <T> void ifPresent(T obj, Consumer<T> proc) {
        if (obj != null) {
            proc.accept(obj);
        }
    }
}