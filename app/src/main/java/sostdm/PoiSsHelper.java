package sostdm;

import java.util.Optional;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;

class PoiSsHelper {

    public static void setStrCellVal(Sheet sheet, int col, int row, String str) {
        ObjUtils.ifPresent(getCell(sheet, col, row), x -> x.setCellValue(str));
    }

    public static void setStrCellVal(Sheet sheet, int col, int row, RichTextString str) {
        ObjUtils.ifPresent(getCell(sheet, col, row), x -> x.setCellValue(str));
    }

    public static Cell getCell(Sheet sheet, int col, int row) {
        if (sheet != null) {
            var row_obj = Optional.ofNullable(sheet.getRow(row)).orElseGet(() -> sheet.createRow(row));
            return Optional.ofNullable(row_obj.getCell(col)).orElseGet(() -> row_obj.createCell(col));
        } else {
            return null;
        }
    }

    public static Optional<String> getCellValStr(Sheet sheet, int col, int row) {
        if (sheet == null) {
            return Optional.empty();
        }

        var row_obj = sheet.getRow(row);
        if (row_obj == null) {
            return Optional.empty();
        }

        var cell_obj = row_obj.getCell(col);
        if (cell_obj == null) {
            return Optional.empty();
        }

        switch (cell_obj.getCellType()) {
            case STRING:
                return Optional.of(cell_obj.getStringCellValue());
            case NUMERIC:
                return Optional.of(Double.toString(cell_obj.getNumericCellValue()));
            case BOOLEAN:
                return Optional.of(Boolean.toString(cell_obj.getBooleanCellValue()));
            case FORMULA:
                return Optional.of(cell_obj.getCellFormula());
            case BLANK:
                return Optional.empty();
            default:
                return Optional.empty();
        }
    }

    public static double getColumnsWidthAsNumOfChrs(Sheet sheet, int start_col, int end_col) {
        if (sheet == null) {
            return 0.0;
        }

        double sum = 0.0;
        for (int col = start_col; col < end_col; ++col) {
            sum += (double) sheet.getColumnWidth(col);
        }

        return sum / 256.0;
    }
}
