package sostdm;

import java.util.Date;
import java.util.Scanner;

import java.io.IOException;
import java.text.SimpleDateFormat;

import com.google.common.base.Strings;

public class App {

    private String stdOutputPath;
    private String linkOutputPath;
    private int paddingLen = 5;

    public App() {
        stdOutputPath = "./dest/std-" + (new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        linkOutputPath = "./dest/link-" + (new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
    }

    public String getAppName() {
        return "sostdm";
    }

    public void enumLink(String start_url) {
        int no = 1;
        StdEntryXlsxMaker maker = new StdEntryXlsxMaker();

        try (var scanner = new Scanner(System.in); var bd = new BrowserDriver()) {

            bd.open(start_url);

            for (;;) {
                System.out.print(
                        "1:enum links and save and new file, 2:append enum links to current file, 3:save and new file, 0:save and exit > ");
                String line = scanner.nextLine();
                if (line == null) {
                    break;
                }

                String str = line.trim();
                if ("0".equals(str)) {
                    break;
                } else if ("1".equals(str)) {
                    bd.enumLink(maker);
                    maker.save(
                            linkOutputPath + "/" + Strings.padStart(Integer.toString(no), paddingLen, '0') + ".xlsx");
                    maker.close();
                    ++no;

                    maker = new StdEntryXlsxMaker();
                } else if ("2".equals(str)) {
                    bd.enumLink(maker);
                } else if ("3".equals(str)) {
                    maker.save(
                            linkOutputPath + "/" + Strings.padStart(Integer.toString(no), paddingLen, '0') + ".xlsx");
                    maker.close();
                    ++no;

                    maker = new StdEntryXlsxMaker();
                }
            }
        } finally {
            if (maker.getNumOfEntries() > 0) {
                maker.save(linkOutputPath + "/" + Strings.padStart(Integer.toString(no), paddingLen, '0') + ".xlsx");
            }
            maker.close();
        }
    }

    public void mkstd(String path) {
        var loader = new StdEntryLoaderXlsx();
        var entries = loader.load(path);

        var std_maker = new StdMaker();
        std_maker.addEntries(entries);

        int no = 1;
        for (StdNode tree : std_maker.createStdTree()) {
            try (var istrm = getClass().getResourceAsStream("/template.xlsx"); var stdmkr = new StdXlsxMaker()) {
                // stdmkr.createNewBook();
                // stdmkr.addNewSheet();
                stdmkr.loadBook(istrm);
                stdmkr.selectSheetAt(0);
                stdmkr.addScreenTransitionDiagram(tree);
                stdmkr.save(stdOutputPath + "/" + Strings.padStart(Integer.toString(no), paddingLen, '0') + ".xlsx");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            no++;
        }

    }

    public void mkstd2(String path) {
        var loader = new StdNodeLoaderXlsxCellTree();
        var trees = loader.load(path);

        int no = 1;
        for (StdNode tree : trees) {
            try (var istrm = getClass().getResourceAsStream("/template.xlsx"); var stdmkr = new StdXlsxMaker()) {
                // stdmkr.createNewBook();
                // stdmkr.addNewSheet();
                stdmkr.loadBook(istrm);
                stdmkr.selectSheetAt(0);
                stdmkr.addScreenTransitionDiagram(tree);
                stdmkr.save(stdOutputPath + "/" + Strings.padStart(Integer.toString(no), paddingLen, '0') + ".xlsx");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            no++;
        }
    }

    public void mkstd3(String path) {
        var loader = new StdNodeLoaderXlsxIntentTree();
        var trees = loader.load(path);

        int no = 1;
        for (StdNode tree : trees) {
            try (var istrm = getClass().getResourceAsStream("/template.xlsx"); var stdmkr = new StdXlsxMaker()) {
                // stdmkr.createNewBook();
                // stdmkr.addNewSheet();
                stdmkr.loadBook(istrm);
                stdmkr.selectSheetAt(0);
                stdmkr.addScreenTransitionDiagram(tree);
                stdmkr.save(stdOutputPath + "/" + Strings.padStart(Integer.toString(no), paddingLen, '0') + ".xlsx");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            no++;
        }

    }

    public static void main(String[] args) {
        var app = new App();

        System.out.println(app.getAppName());

        if (args.length < 1) {
            putHelp();
            return;
        }

        String command = args[0];
        if ("enumlink".equals(command)) {
            if (args.length < 2) {
                System.err.println("no url");
                return;
            }

            app.enumLink(args[1]);

        } else if ("mkstd".equals(command)) {
            if (args.length < 2) {
                System.err.println("no input file");
                return;
            }

            app.mkstd(args[1]);

        } else if ("mkstd2".equals(command)) {
            if (args.length < 2) {
                System.err.println("no input file");
                return;
            }

            app.mkstd2(args[1]);

        } else if ("mkstd3".equals(command)) {
            if (args.length < 2) {
                System.err.println("no input file");
                return;
            }

            app.mkstd3(args[1]);

        } else if ("testwrap".equals(command)) {
            String test_str = "これは、ワードラップの確認です。ワードラップとは単語を途中で切らないように行を折り返すことです。";
            if (args.length >= 2) {
                test_str = args[1];
            }

            WordWrapperForJapanese.testStart(test_str);

        } else if ("testwrap2".equals(command)) {
            String test_str = "これは、ワードラップの確認です。ワードラップとは単語を途中で切らないように行を折り返すことです。";
            if (args.length >= 2) {
                test_str = args[1];
            }

            double cols = 25.0;
            if (args.length >= 3) {
                cols = Double.parseDouble(args[2]);
            }

            double fullwidth_rate = 2.0;
            if (args.length >= 4) {
                fullwidth_rate = Double.parseDouble(args[3]);
            }

            WordWrapperForJapanese.testStart2(test_str, cols, fullwidth_rate);

        } else {
            putHelp();
        }
    }

    public static void putHelp() {
        System.err.println("usage:");
        System.err.println("    gradle run --args=\"enumlink https://example.com/\"");
        System.err.println("    gradle run --args=\"mkstd example/screen_transition_list.xlsx\"");
        System.err.println("    gradle run --args=\"mkstd2 example/tree_list.xlsx\"");
        System.err.println("    gradle run --args=\"mkstd3 example/tree_list2.xlsx\"");
    }
}
