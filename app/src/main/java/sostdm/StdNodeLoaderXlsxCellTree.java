package sostdm;

import java.util.List;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.commons.lang3.StringUtils;

public class StdNodeLoaderXlsxCellTree extends StdNodeLoaderXlsx {

    private String sheet_name = null;
    private int sheet_index = 0;
    private int ignore_rows = 1;

    private boolean screen_val1_is_name = true;
    private int screen_val1_col_begin = 0;
    private int screen_val1_col_end = 10;
    private int screen_val2_col = screen_val1_col_end;
    private int event_name_col = screen_val1_col_end + 1;
    private Integer enable_flag_col = screen_val1_col_end + 2;
    private List<Integer> screen_type_cols = Arrays.asList(screen_val1_col_end + 3);
    private List<Integer> transition_type_cols = Arrays.asList(screen_val1_col_end + 4);
    private Integer screen_remark_col = screen_val1_col_end + 5;
    private Integer transition_remark_col = screen_val1_col_end + 6;

    public StdNodeLoaderXlsxCellTree() {
    }

    protected String getSheetName() {
        return this.sheet_name;
    }

    protected int getSheetIndex() {
        return this.sheet_index;
    }

    protected int getIgnoreRows() {
        return this.ignore_rows;
    }

    protected boolean getScreenVal1IsName() {
        return this.screen_val1_is_name;
    }

    protected int getScreenVal2Col() {
        return this.screen_val2_col;
    }

    protected int getEventNameCol() {
        return this.event_name_col;
    }

    protected Integer getEnableFlagCol() {
        return this.enable_flag_col;
    }

    protected List<Integer> getScreenTypeCols() {
        return screen_type_cols;
    }

    protected Integer getScreenRemarkCol() {
        return screen_remark_col;
    }

    protected List<Integer> getTransitionTypeCols() {
        return transition_type_cols;
    }

    protected Integer getTransitionRemarkCol() {
        return transition_remark_col;
    }

    protected ValDepth getCellValueStringDepth(Sheet sheet, int row) {
        for (var col_cnt = this.screen_val1_col_begin; col_cnt < this.screen_val1_col_end; ++col_cnt) {
            final var col = col_cnt;
            var result = PoiSsHelper.getCellValStr(sheet, col, row)
                    .map(val -> StringUtils.isNotEmpty(val) ? new ValDepth(val, 1 + col - this.screen_val1_col_begin)
                            : null);
            if (result.isPresent()) {
                return result.get();
            }
        }
        return null;
    }
}
