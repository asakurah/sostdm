package sostdm;

public class StdEntry {
    public String fromName;
    public String fromKey;
    public String event;
    public String toName;
    public String toKey;

    public StdEntry(String fromName, String fromKey, String event, String toName, String toKey) {
        this.fromName = fromName;
        this.fromKey = fromKey;
        this.event = event;
        this.toName = toName;
        this.toKey = toKey;
    }
}
