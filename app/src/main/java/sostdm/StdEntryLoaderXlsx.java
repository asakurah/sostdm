package sostdm;

import java.io.File;
import java.util.List;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;

import com.google.common.base.Strings;

public class StdEntryLoaderXlsx {

    private String sheet_name = null;
    private int sheet_index = 0;
    private int ignore_rows = 1;
    private int from_screen_name_col = 0;
    private int from_screen_key_col = 1;
    private int event_name_col = 2;
    private int to_screen_name_col = 3;
    private int to_screen_key_col = 4;
    private Integer enable_flag_col = 5;

    public StdEntryLoaderXlsx() {
    }

    public List<StdEntry> load(String path) {
        var entries = new ArrayList<StdEntry>();

        try (Workbook wb = WorkbookFactory.create(new File(path), null, true)) {

            var sheet = getSheet(wb);

            int lastRow = sheet.getLastRowNum();

            for (int row = ignore_rows; row <= lastRow; ++row) {
                String from_name = PoiSsHelper.getCellValStr(sheet, from_screen_name_col, row).orElse("");
                String from_key = PoiSsHelper.getCellValStr(sheet, from_screen_key_col, row).orElse("");
                String event_name = PoiSsHelper.getCellValStr(sheet, event_name_col, row).orElse("");
                String to_name = PoiSsHelper.getCellValStr(sheet, to_screen_name_col, row).orElse("");
                String to_key = PoiSsHelper.getCellValStr(sheet, to_screen_key_col, row).orElse("");
                boolean is_enabled = strToBool(PoiSsHelper.getCellValStr(sheet, enable_flag_col, row).orElse(""));

                if (is_enabled) {
                    if (!Strings.isNullOrEmpty(from_key)) {
                        var entry = new StdEntry(strOrDefault(from_name, from_key), from_key, event_name,
                                strOrDefault(to_name, to_key), to_key);
                        entries.add(entry);
                    }
                }
            }

        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }

        return entries;
    }

    private Sheet getSheet(Workbook wb) {
        if (sheet_name != null) {
            return wb.getSheet(sheet_name);
        } else {
            return wb.getSheetAt(sheet_index);
        }
    }

    private boolean strToBool(String src) {
        if (src == null) {
            return false;
        }
        String src_lc = src.toLowerCase();
        try {
            double d = Double.parseDouble(src_lc);
            return !(Double.isNaN(d) || Double.isInfinite(d) || d == 0.0);
        } catch (Throwable t) {
            return "y".equals(src_lc) || "yes".equals(src_lc) || "t".equals(src_lc) || "true".equals(src_lc);
        }
    }

    private String strOrDefault(String val, String def_val) {
        return Strings.isNullOrEmpty(val) ? def_val : val;
    }
}
