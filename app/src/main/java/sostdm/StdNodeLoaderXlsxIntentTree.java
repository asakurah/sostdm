package sostdm;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.commons.lang3.StringUtils;

public class StdNodeLoaderXlsxIntentTree extends StdNodeLoaderXlsx {

    private String sheet_name = null;
    private int sheet_index = 0;
    private int ignore_rows = 1;

    private boolean screen_val1_is_name = true;
    private int screen_val1_col = 0;
    private int screen_val2_col = 1;
    private int event_name_col = 2;
    private Integer enable_flag_col = 3;

    public StdNodeLoaderXlsxIntentTree() {
    }

    protected String getSheetName() {
        return this.sheet_name;
    }

    protected int getSheetIndex() {
        return this.sheet_index;
    }

    protected int getIgnoreRows() {
        return this.ignore_rows;
    }

    protected boolean getScreenVal1IsName() {
        return this.screen_val1_is_name;
    }

    protected int getScreenVal2Col() {
        return this.screen_val2_col;
    }

    protected int getEventNameCol() {
        return this.event_name_col;
    }

    protected Integer getEnableFlagCol() {
        return this.enable_flag_col;
    }

    protected List<Integer> getScreenTypeCols() {
        return null;
    }

    protected Integer getScreenRemarkCol() {
        return null;
    }

    protected List<Integer> getTransitionTypeCols() {
        return null;
    }

    protected Integer getTransitionRemarkCol() {
        return null;
    }

    protected ValDepth getCellValueStringDepth(Sheet sheet, int row) {
        final int col = this.screen_val1_col;
        return PoiSsHelper.getCellValStr(sheet, col, row).map(val->{
            if (StringUtils.isNotEmpty(val)) {
                var cell = sheet.getRow(row).getCell(col);
                var style = cell.getCellStyle();
                return new ValDepth(val, (int) style.getIndention());
            } else {
                return null;
            }    
        }).orElse(null);
    }

}
