package sostdm;

import org.apache.commons.lang3.StringUtils;

public class StdRelation {
    public String event;
    public String key;
    public AttrSet attr_set = new AttrSet();

    public boolean equals(StdRelation obj) {
        return toInternalString(this).equals(toInternalString(obj));
    }

    public int hashCode() {
        return toInternalString(this).hashCode();
    }

    private static String toInternalString(StdRelation obj) {
        if (obj == null) {
            return "";
        } else {
            StringBuffer buf = new StringBuffer();
            buf.append(StringUtils.defaultString(obj.key));
            for (var elm : obj.attr_set.types) {
                buf.append('\t');
                buf.append(StringUtils.defaultString(elm));
            }
            return buf.toString();
        }
    }
}
