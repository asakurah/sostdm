package sostdm;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Stack;

import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;

import org.apache.commons.lang3.StringUtils;

public abstract class StdNodeLoaderXlsx {

    protected abstract String getSheetName();

    protected abstract int getSheetIndex();

    protected abstract int getIgnoreRows();

    protected abstract boolean getScreenVal1IsName();

    protected abstract int getScreenVal2Col();

    protected abstract int getEventNameCol();

    protected abstract Integer getEnableFlagCol();

    protected abstract List<Integer> getScreenTypeCols();

    protected abstract Integer getScreenRemarkCol();

    protected abstract List<Integer> getTransitionTypeCols();

    protected abstract Integer getTransitionRemarkCol();

    protected abstract ValDepth getCellValueStringDepth(Sheet sheet, int row);

    protected record ValDepth(String val, int depth) {
    }

    protected record NodeDepth(StdNode node, int depth) {
    }

    protected IdGenerator nodeId = new IdGenerator();

    public List<StdNode> load(String path) {
        var trees = new ArrayList<StdNode>();

        try (Workbook wb = WorkbookFactory.create(new File(path), null, true)) {

            var sheet = getSheet(wb);

            int lastRow = sheet.getLastRowNum();

            var stack = new Stack<NodeDepth>();

            for (int row = this.getIgnoreRows(); row <= lastRow; ++row) {
                var val1_depth = getCellValueStringDepth(sheet, row);
                if (val1_depth == null) {
                    continue;
                }

                Integer enable_flag_col = this.getEnableFlagCol();
                boolean is_enabled = enable_flag_col != null
                        ? strToBool(PoiSsHelper.getCellValStr(sheet, enable_flag_col, row).orElse(""))
                        : true;
                if (!is_enabled) {
                    continue;
                }

                int depth = val1_depth.depth;
                String val1 = val1_depth.val;
                String val2 = PoiSsHelper.getCellValStr(sheet, this.getScreenVal2Col(), row).orElse(null);
                String event_name = PoiSsHelper.getCellValStr(sheet, this.getEventNameCol(), row).orElse(null);

                var node = new StdNode();
                if (this.getScreenVal1IsName()) {
                    node.name = val1;
                    node.key = StringUtils.defaultString(val2, val1);
                } else {
                    node.key = val1;
                    node.name = StringUtils.defaultString(val2, val1);
                }
                node.index = nodeId.next();
                node.height = 1;
                this.getCols(node.attrSet.types, sheet, this.getScreenTypeCols(), row);
                this.getCol(sheet, this.getScreenRemarkCol(), row).ifPresent(x -> {
                    node.attrSet.remark = x;
                });

                var rel = new StdRelation();
                rel.event = event_name;
                rel.key = node.key;
                getCols(rel.attr_set.types, sheet, this.getTransitionTypeCols(), row);
                getCol(sheet, this.getTransitionRemarkCol(), row).ifPresent(x -> {
                    rel.attr_set.remark = x;
                });

                if (stack.size() > 0) {
                    StdNode child = null;

                    var nd = stack.peek();
                    for (; nd.depth >= depth; nd = stack.peek()) {
                        var parent = nd.node;
                        parent.endIndex = nodeId.current();
                        addHeight(parent, child);
                        child = parent;
                        stack.pop();
                    }

                    var parent = nd.node;
                    addHeight(parent, child);
                    parent.children.put(rel, node);
                } else {
                    trees.add(node);
                }

                stack.push(new NodeDepth(node, depth));
            }

        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }

        return trees;
    }

    protected Sheet getSheet(Workbook wb) {
        var sheet_name = this.getSheetName();
        if (sheet_name != null) {
            return wb.getSheet(sheet_name);
        } else {
            return wb.getSheetAt(this.getSheetIndex());
        }
    }

    protected void getCols(List<String> dst, Sheet sheet, List<Integer> cols, int row) {
        if (cols == null) {
            return;
        }

        for (Integer col : cols) {
            String val = "";
            if (col != null) {
                val = PoiSsHelper.getCellValStr(sheet, col, row).orElse("");
            }

            dst.add(val);
        }
    }

    protected Optional<String> getCol(Sheet sheet, Integer col, int row) {
        if (col != null) {
            return PoiSsHelper.getCellValStr(sheet, col, row);
        } else {
            return Optional.empty();
        }
    }

    protected boolean strToBool(String src) {
        if (src == null) {
            return false;
        }
        String src_lc = src.toLowerCase();
        try {
            double d = Double.parseDouble(src_lc);
            return !(Double.isNaN(d) || Double.isInfinite(d) || d == 0.0);
        } catch (Throwable t) {
            return "y".equals(src_lc) || "yes".equals(src_lc) || "t".equals(src_lc) || "true".equals(src_lc);
        }
    }

    protected void addHeight(StdNode parent, StdNode child) {
        if (child != null) {
            if (parent.children.size() == 1) {
                --parent.height;
            }

            parent.height += child.height;
        }
    }
}
