package sostdm;

record XlsxConnectorPercent(int value) {
    public static XlsxConnectorPercent from(double value) {
        return new XlsxConnectorPercent((int) Math.round(value * 1000.0));
    }

    public double toRate() {
        return ((double) this.value) / 100000.0;
    }
}