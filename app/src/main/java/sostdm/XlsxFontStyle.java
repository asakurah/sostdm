package sostdm;

import org.apache.commons.lang3.ObjectUtils;

import org.apache.poi.ss.usermodel.FontUnderline;

record XlsxFontStyle(String font_name, XlsxFontHeight font_height, Boolean bold, Boolean italic, Boolean strikeout,
        FontUnderline underline) implements Overwritable<XlsxFontStyle> {

    public XlsxFontStyle overwrite(XlsxFontStyle obj) {
        if (obj == null) {
            return this;
        }

        return new XlsxFontStyle(ObjectUtils.defaultIfNull(obj.font_name, this.font_name),
                ObjectUtils.defaultIfNull(obj.font_height, this.font_height),
                ObjectUtils.defaultIfNull(obj.bold, this.bold), ObjectUtils.defaultIfNull(obj.italic, this.italic),
                ObjectUtils.defaultIfNull(obj.strikeout, this.strikeout),
                ObjectUtils.defaultIfNull(obj.underline, this.underline));
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String font_name;
        private XlsxFontHeight font_height;
        private Boolean bold;
        private Boolean italic;
        private Boolean strikeout;
        private FontUnderline underline;

        public Builder() {
        }

        public XlsxFontStyle build() {
            return new XlsxFontStyle(this.font_name, this.font_height, this.bold, this.italic, this.strikeout,
                    this.underline);
        }

        public Builder font_name(String v) {
            this.font_name = v;
            return this;
        }

        public Builder font_height(XlsxFontHeight v) {
            this.font_height = v;
            return this;
        }

        public Builder bold(Boolean v) {
            this.bold = v;
            return this;
        }

        public Builder italic(Boolean v) {
            this.italic = v;
            return this;
        }

        public Builder strikeout(Boolean v) {
            this.strikeout = v;
            return this;
        }

        public Builder underline(FontUnderline v) {
            this.underline = v;
            return this;
        }

    }

}
