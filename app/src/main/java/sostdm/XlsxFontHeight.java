package sostdm;

record XlsxFontHeight(short value) {
    public static XlsxFontHeight from(double value) {
        return new XlsxFontHeight((short) Math.round(value * 20));
    }
}
