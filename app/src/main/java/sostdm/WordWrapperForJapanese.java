package sostdm;

import java.util.List;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.atilika.kuromoji.Token;
import org.atilika.kuromoji.Tokenizer;

public class WordWrapperForJapanese {

    private String illegal_head_chrs = "、。！？：；,.!?:;､｡" + ")]}>」』）｝】＞≫］”’" + "ぁぃぅぇぉっゃゅょァィゥェォッャュョｧｨｩｪｫｯｬｭｮ" + "／・ー―/-･ｰ"
            + "ゝ々";
    private String illegal_tail_chrs = "([{<「『（｛【＜≪［“‘";

    private Tokenizer tokenizer = Tokenizer.builder().build();

    public WordWrapperForJapanese() {
    }

    public List<String> wordWrap(String src, double chrs, double fullwidth_rate) {
        List<String> buf = new ArrayList<String>();
        if (src == null || src.isEmpty() || src.isBlank()) {
            return buf;
        }

        for (String line : src.split("\n|\r|\n\r")) {
            wordWrapLine(buf, line.trim(), chrs, fullwidth_rate);
        }

        return buf;
    }

    protected void wordWrapLine(List<String> dst, String src, double line_width, double fullwidth_rate) {
        if (src.isEmpty()) {
            dst.add("");
            return;
        }

        StringBuffer line = new StringBuffer();
        double text_width = 0.0;
        for (String part : splitByWord(src)) {
            double part_width = getWidth(part, fullwidth_rate);
            double new_text_width = text_width + part_width;
            if (new_text_width > line_width) {
                dst.add(line.toString());
                line.delete(0, line.length());
                text_width = 0.0;
            }
            line.append(part);
            text_width += part_width;
        }
        dst.add(line.toString());
    }

    protected List<String> splitByWord(String src) {
        List<String> words = new ArrayList<String>();
        List<Token> tokens = tokenizer.tokenize(src);
        StringBuffer word = new StringBuffer();
        for (Token token : tokens) {
            if (!isIllegalHeadChr(token) && !isIllegalTailChr(word.toString())) {
                words.add(word.toString());
                word.delete(0, word.length());
            }
            word.append(token.getSurfaceForm());
        }
        if (word.length() > 0) {
            words.add(word.toString());
        }
        return words;
    }

    protected boolean isIllegalHeadChr(Token token) {
        var part = token.getSurfaceForm();
        var chr_end_idx = part.offsetByCodePoints(0, 1);
        var attr = token.getPartOfSpeech();
        return attr.startsWith("助") || this.illegal_head_chrs.indexOf(part.substring(0, chr_end_idx)) >= 0;
    }

    protected boolean isIllegalTailChr(String part) {
        var num_of_cps = part.codePointCount(0, part.length());
        var chr_start_idx = num_of_cps > 1 ? part.offsetByCodePoints(0, num_of_cps - 1) : 0;
        var chr_end_idx = part.offsetByCodePoints(0, num_of_cps);
        return this.illegal_tail_chrs.indexOf(part.substring(chr_start_idx, chr_end_idx)) >= 0;
    }

    protected double getWidth(String src, double fullwidth_rate) {
        double cnt = 0.0;
        try {
            for (String c : src.split("")) {
                if (c.getBytes("MS932").length > 1) {
                    cnt += fullwidth_rate;
                } else {
                    cnt += 1;
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return cnt;
    }

    public static void testStart(String str) {
        Tokenizer tokenizer = Tokenizer.builder().build();
        List<Token> tokens = tokenizer.tokenize(str);
        for (Token token : tokens) {
            System.out.println("");
            System.out.println("表層:" + token.getSurfaceForm());
            System.out.println("語幹:" + token.getBaseForm());
            System.out.println("読み:" + token.getReading());
            System.out.println("属性:" + token.getAllFeatures());
            try {
                System.out.println("utf8 bytes:" + token.getSurfaceForm().getBytes("UTF-8").length);
                System.out.println("sjis bytes:" + token.getSurfaceForm().getBytes("MS932").length);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

    public static void testStart2(String src, double cols, double fullwidth_rate) {
        var ww = new WordWrapperForJapanese();
        for (String line : ww.wordWrap(src, cols, fullwidth_rate)) {
            System.out.println(line);
        }
    }
}