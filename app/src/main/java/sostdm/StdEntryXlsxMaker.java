package sostdm;

import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.ClientAnchor.AnchorType;

public class StdEntryXlsxMaker implements AutoCloseable {

    private Workbook cur_wb = null;
    private Sheet cur_sheet = null;

    private int curRow;
    private int numOfEntries = 0;

    public StdEntryXlsxMaker() {
        try {
            cur_wb = WorkbookFactory.create(true);
            cur_sheet = cur_wb.createSheet("Screen Transition List");
            Row r = cur_sheet.createRow(0);
            r.createCell(0, CellType.STRING).setCellValue("From Screen Name");
            r.createCell(1, CellType.STRING).setCellValue("From URL");
            r.createCell(2, CellType.STRING).setCellValue("Event Name");
            r.createCell(3, CellType.STRING).setCellValue("To Screen Name");
            r.createCell(4, CellType.STRING).setCellValue("To URL");
            r.createCell(5, CellType.STRING).setCellValue("Is Enabled");
            // r.createCell(6, CellType.STRING).setCellValue("Element Image");
            curRow = 1;
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public int getNumOfEntries() {
        return this.numOfEntries;
    }

    public void add(String fromName, String fromKey, String event, String toName, String toKey, boolean isEnabled,
            byte[] image, String additional) {
        Row r = cur_sheet.createRow(curRow);
        r.createCell(0, CellType.STRING).setCellValue(fromName);
        r.createCell(1, CellType.STRING).setCellValue(fromKey);
        r.createCell(2, CellType.STRING).setCellValue(event);
        r.createCell(3, CellType.STRING).setCellValue(toName);
        r.createCell(4, CellType.STRING).setCellValue(toKey);
        r.createCell(5, CellType.STRING).setCellValue(isEnabled ? "true" : "false");
        if (image != null) {
            putImage(6, curRow, image);
        }
        if (additional != null) {
            r.createCell(7, CellType.STRING).setCellValue(additional);
        }
        ++curRow;
        ++numOfEntries;
    }

    public void putImage(int col, int row, byte[] image) {
        try {
            int pic_idx = cur_wb.addPicture(image, Workbook.PICTURE_TYPE_PNG);

            var patriarch = cur_sheet.createDrawingPatriarch();

            ClientAnchor anchor = patriarch.createAnchor(0, 0, 0, 0, col, row, col + 1, row + 1);

            anchor.setAnchorType(AnchorType.MOVE_DONT_RESIZE);

            patriarch.createPicture(anchor, pic_idx);

        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void save(String path) {
        if (cur_wb == null) {
            return;
        }

        try {
            Path dst_path = FileSystems.getDefault().getPath(path);
            Files.createDirectories(dst_path.getParent());
            try (OutputStream fos = Files.newOutputStream(dst_path)) {
                cur_wb.write(fos);
                fos.close();
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public void close() throws RuntimeException {
        if (cur_wb == null) {
            return;
        }

        try {
            cur_wb.close();
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

}
