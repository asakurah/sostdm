package sostdm;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

class StdMaker {

    private class StdTmpNode {
        public String name;
        public String key;
        public boolean isSearched;
        public LinkedHashSet<StdRelation> children = new LinkedHashSet<StdRelation>();
    }

    private LinkedHashMap<String, StdTmpNode> entryMap = new LinkedHashMap<String, StdTmpNode>();
    private int index = 1;

    public StdMaker() {
    }

    public void addEntries(List<StdEntry> entries) {
        for (StdEntry entry : entries) {
            this.addEntry(entry);
        }
    }

    public void addEntry(StdEntry entry) {
        var rel = new StdRelation();
        rel.event = entry.event;
        rel.key = entry.toKey;

        var from_node = entryMap.get(entry.fromKey);
        if (from_node != null) {
            if (rel != null) {
                from_node.children.add(rel);
            }
        } else {
            from_node = new StdTmpNode();
            from_node.name = entry.fromName;
            from_node.key = entry.fromKey;
            if (rel != null) {
                from_node.children.add(rel);
            }
            entryMap.put(entry.fromKey, from_node);
        }

        if (!entryMap.containsKey(entry.toKey)) {
            var to_node = new StdTmpNode();
            to_node.name = entry.toName;
            to_node.key = entry.toKey;
            to_node.children = new LinkedHashSet<StdRelation>();
            entryMap.put(entry.toKey, to_node);
        }
    }

    public List<StdNode> createStdTree() {
        var trees = new ArrayList<StdNode>();

        if (entryMap.size() < 1) {
            return trees;
        }

        String key;
        while ((key = getNotSerched()) != null) {
            var tree = searchEntryMap(key);
            if (tree != null) {
                trees.add(tree);
            }
        }

        return trees;
    }

    private String getNotSerched() {
        for (var pair : entryMap.entrySet()) {
            var entry = pair.getValue();
            if (!entry.isSearched) {
                return pair.getKey();
            }
        }
        return null;
    }

    private StdNode searchEntryMap(String key) {
        var tmp_node = entryMap.get(key);
        if (tmp_node == null) {
            System.err.printf("key '%s' is nothing.\n", key);
            return null;
        }

        var node = new StdNode();
        node.name = tmp_node.name;
        node.key = tmp_node.key;
        node.index = getNextIndex();

        if (tmp_node.isSearched) {
            node.isSeeOthers = true;
            node.endIndex = seeCurrentIndex();
            node.height = 1;
            return node;
        }

        tmp_node.isSearched = true;

        for (var rel : tmp_node.children) {
            var child = searchEntryMap(rel.key);
            if (child != null) {
                node.children.put(rel, child);
                node.height += child.height;
            }
        }

        if (tmp_node.children.size() < 1) {
            node.height = 1;
        }

        node.endIndex = seeCurrentIndex();
        return node;
    }

    private int getNextIndex() {
        int ret_index = this.index;
        this.index++;
        return ret_index;
    }

    private int seeCurrentIndex() {
        return this.index;
    }
}
