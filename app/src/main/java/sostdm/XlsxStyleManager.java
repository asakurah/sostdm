package sostdm;

import java.util.HashMap;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFTextParagraph;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

import org.apache.commons.lang3.StringUtils;

class XlsxStyleManager {

    private XSSFWorkbook workbook;

    private HashMap<XlsxFontStyle, XSSFFont> font_map = new HashMap<XlsxFontStyle, XSSFFont>();

    public XlsxStyleManager(Workbook workbook) {
        if (workbook instanceof XSSFWorkbook) {
            this.workbook = (XSSFWorkbook) workbook;
        } else {
            throw new RuntimeException("Illegal type");
        }
    }

    public XlsxStyleManager(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    public XSSFFont getFont(XlsxFontStyle font_style) {
        if (font_map.containsKey(font_style)) {
            return font_map.get(font_style);
        }

        final XSSFFont font = workbook.createFont();
        ObjUtils.ifPresent(font_style.font_name(), x -> font.setFontName(x));
        ObjUtils.ifPresent(font_style.font_height(), x -> font.setFontHeight(x.value()));
        ObjUtils.ifPresent(font_style.bold(), x -> font.setBold(x));
        ObjUtils.ifPresent(font_style.italic(), x -> font.setItalic(x));
        ObjUtils.ifPresent(font_style.strikeout(), x -> font.setStrikeout(x));
        ObjUtils.ifPresent(font_style.underline(), x -> font.setUnderline(x));

        font_map.put(font_style, font);

        return font;
    }

    public void applyStyleAndText(XSSFSimpleShape obj, XlsxShapeStyle style, String text) {
        if (obj == null) {
            return;
        }

        if (style != null) {
            String tmp_text = StringUtils.defaultString(style.force_text(), text);

            ObjUtils.ifPresent(style.fill_color(), x -> x.apply(r -> g -> b -> obj.setFillColor(r, g, b)));
            ObjUtils.ifPresent(style.line_color(), x -> x.apply(r -> g -> b -> obj.setLineStyleColor(r, g, b)));
            XlsxShapeHelper.applyLineStyle(style.line_style(), x -> obj.setLineStyle(x));
            ObjUtils.ifPresent(style.line_width(), x -> obj.setLineWidth(x));

            ObjUtils.ifPresent(style.h_overflow(), x -> obj.setTextHorizontalOverflow(x));
            ObjUtils.ifPresent(style.v_overflow(), x -> obj.setTextVerticalOverflow(x));

            if (tmp_text != null) {
                obj.clearText();

                var paragraph = addNewTextParagraph(obj, style.font(), tmp_text);
                ObjUtils.ifPresent(style.h_align(), x -> paragraph.setTextAlign(x));

                ObjUtils.ifPresent(style.v_align(), x -> obj.setVerticalAlignment(x));
            }

        } else if (text != null) {
            obj.clearText();
            obj.addNewTextParagraph(text);
        }
    }

    public XSSFTextParagraph addNewTextParagraph(XSSFSimpleShape obj, XlsxFontStyle font, String text) {
        if (font == null) {
            return obj.addNewTextParagraph(text);
        } else {
            var rich_text = new XSSFRichTextString(text);
            rich_text.applyFont(this.getFont(font));
            return obj.addNewTextParagraph(rich_text);
        }
    }
}
